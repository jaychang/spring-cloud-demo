package com.choosefine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class ApiGatewayTradionalMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayTradionalMsApplication.class, args);
	}
}
