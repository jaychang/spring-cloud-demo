package com.choosefine.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/22
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@EnableBinding(value = {Sink.class})
public class SinkReceiver1 {
    private static final Logger log = LoggerFactory.getLogger(SinkReceiver1.class);

    @StreamListener(value = Sink.INPUT)
    public void receive(Object payload){
        log.info("Received:"+payload);
    }
}
