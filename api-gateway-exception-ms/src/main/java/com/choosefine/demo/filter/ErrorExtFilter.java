package com.choosefine.demo.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.cloud.netflix.zuul.filters.post.SendErrorFilter;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/14
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class ErrorExtFilter extends SendErrorFilter {

    @Override
    public boolean shouldFilter() {
       //TODO 来自post过滤器抛出的异常
        RequestContext currentContext = RequestContext.getCurrentContext();
        Object o = currentContext.get("failed.filter");
        return null != o && "post".equals(((ZuulFilter)o).filterType());
    }

    @Override
    public String filterType() {
        return "error";
    }

    @Override
    public int filterOrder() {
        return 30;
    }

}
