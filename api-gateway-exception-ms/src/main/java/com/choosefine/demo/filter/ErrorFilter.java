package com.choosefine.demo.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/14
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class ErrorFilter extends ZuulFilter {
    @Override
    public String filterType() {
        return "error";
    }

    @Override
    public int filterOrder() {
        return 10;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        //主要用于设置error.status_code，error.exception便于SendErrorFilter及其子类（我们这里定义了ErrorExtFilter，filterType为error）处理
        RequestContext currentContext = RequestContext.getCurrentContext();
        Throwable throwable = currentContext.getThrowable();
        currentContext.set("error.status_code", HttpStatus.INTERNAL_SERVER_ERROR.value());
        currentContext.set("error.exception",throwable.getCause());
        currentContext.set("error.message","error occur "+throwable.getMessage());
        return null;
    }
}
