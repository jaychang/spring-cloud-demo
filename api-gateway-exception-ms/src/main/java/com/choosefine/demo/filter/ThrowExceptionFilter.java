package com.choosefine.demo.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/14
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class ThrowExceptionFilter extends ZuulFilter {
    private static final Logger logger = LoggerFactory.getLogger(ThrowExceptionFilter.class);
    @Override
    public String filterType() {
        return "post";
    }

    @Override
    public int filterOrder() {
        return 100;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        logger.info("It is a post ZuulFilter,it will throw a Exception.");
        //try {
            doSome();
//        }catch (Exception e){
//            RequestContext currentContext = RequestContext.getCurrentContext();
//            currentContext.set("error.status_code", HttpStatus.INTERNAL_SERVER_ERROR.value());
//            currentContext.set("error.message","Some error occur!");
//            currentContext.set("error.exception",e);
//        }
        return null;
    }

    public void doSome(){
        throw new RuntimeException("doSome occur exception");
    }
}
