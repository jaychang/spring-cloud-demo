package com.choosefine.demo;

import com.choosefine.demo.filterprocess.MyFilterProcess;
import com.netflix.zuul.FilterProcessor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.context.annotation.Bean;

@EnableZuulProxy
@SpringBootApplication
public class ApiGatewayExceptionMsApplication {

	public static void main(String[] args) {
		FilterProcessor.setProcessor(new MyFilterProcess());
		SpringApplication.run(ApiGatewayExceptionMsApplication.class, args);
	}
}
