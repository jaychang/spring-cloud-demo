package com.choosefine.demo.filterprocess;

import com.netflix.zuul.FilterProcessor;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/14
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class MyFilterProcess extends FilterProcessor {

    @Override
    public Object processZuulFilter(ZuulFilter filter) throws ZuulException {
        try{
            return super.processZuulFilter(filter);
        }catch (ZuulException e){
            RequestContext currentContext = RequestContext.getCurrentContext();
            currentContext.set("failed.filter",filter);
            throw e;
        }
    }
}
