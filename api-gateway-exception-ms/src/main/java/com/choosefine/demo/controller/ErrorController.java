package com.choosefine.demo.controller;

import com.netflix.zuul.context.RequestContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/14
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
//@RestController
public class ErrorController {
    @GetMapping("error")
    public ResponseEntity<Map<String,Object>> error(){
        RequestContext currentContext = RequestContext.getCurrentContext();
        Map<String,Object> result = new HashMap<String,Object>();
        result.put("code",1000);
        result.put("message",currentContext.get("error.message"));
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(result);
    }
}
