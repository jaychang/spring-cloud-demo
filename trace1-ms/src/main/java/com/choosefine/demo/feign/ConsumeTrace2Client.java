package com.choosefine.demo.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@FeignClient(name = "trace2-ms",fallback = ConsumeTrace2Client.ConsumeTrace2Fallback.class)
public interface ConsumeTrace2Client {
    @GetMapping("bar")
    String consumeTrace2();

    @Component
    public static class ConsumeTrace2Fallback implements ConsumeTrace2Client{

        @Override
        public String consumeTrace2() {
            return "consume trace2 error";
        }
    }
}
