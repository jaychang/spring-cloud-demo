package com.choosefine.demo.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@FeignClient(name = "trace5-ms",fallback = ConsumeTrace5Client.ConsumeTrace5Fallback.class)
public interface ConsumeTrace5Client {
    @GetMapping("quux")
    String consumeTrace5();

    @Component
    public static class ConsumeTrace5Fallback implements ConsumeTrace5Client {

        @Override
        public String consumeTrace5() {
            return "consume trace5 error";
        }
    }
}
