package com.choosefine.component;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class Sender {
    @Autowired
    private AmqpTemplate amqpTemplate;

    public void send(String name){
        String content = "hello "+name +" now is "+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        amqpTemplate.convertAndSend("hello1",content);
    }
}
