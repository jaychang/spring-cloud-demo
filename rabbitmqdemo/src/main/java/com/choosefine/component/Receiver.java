package com.choosefine.component;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
@RabbitListener(queues = {"hello1"})
public class Receiver {

    @RabbitHandler
    public void receive(String content){
        System.out.println("Receiver.receive");
        System.out.println("received msg :"+content);
    }
}
