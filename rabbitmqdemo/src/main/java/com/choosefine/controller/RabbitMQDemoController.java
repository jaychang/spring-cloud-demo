package com.choosefine.controller;

import com.choosefine.component.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/10
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class RabbitMQDemoController {
    @Autowired
    private Sender sender;
    @GetMapping("send")
    public String send(@RequestParam(name = "name",defaultValue = "annoymous") String name){
        System.out.println("RabbitMQDemoController.send");
        sender.send(name);
        return "send ok";
    }
}
