package com.choosefine.demo.controller;

import com.choosefine.demo.feign.ConsumeTrace2Client;
import com.choosefine.demo.feign.ConsumeTrace5Client;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class Trade1DemoController {
    private static final Logger log = LoggerFactory.getLogger(Trade1DemoController.class);

    @Autowired
    private ConsumeTrace2Client consumeTrace2Client;
    @Autowired
    private ConsumeTrace5Client consumeTrace5Client;

    @GetMapping("foo")
    public String trace1(HttpServletRequest request){
        log.debug("SpanName={},TradeId={},SpanId={},ParentSpanId={}",request.getHeader("x-span-name"),request.getHeader("x-b3-traceid"),request.getHeader("x-b3-spanid"),request.getHeader("x-b3-parentspanid"));
        String trace2Result = consumeTrace2Client.consumeTrace2();
        String trace5Result = consumeTrace5Client.consumeTrace5();
        return trace2Result+"|"+trace5Result;
    }
}
