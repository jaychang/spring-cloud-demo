package com.choosefine.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.turbine.EnableTurbine;

@EnableTurbine
@SpringBootApplication
public class HystrixTurbineMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(HystrixTurbineMsApplication.class, args);
	}
}