package com.choosefine.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringSecurityOauth01Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringSecurityOauth01Application.class, args);
	}
}
