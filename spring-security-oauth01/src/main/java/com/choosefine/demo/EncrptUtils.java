package com.choosefine.demo;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.crypto.password.StandardPasswordEncoder;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/23
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class EncrptUtils {
    private static final String SITE_WIDE_SECRECT = "jaychang";

    private static final PasswordEncoder passwordEncoder = new StandardPasswordEncoder(SITE_WIDE_SECRECT);

    public static String encrpt(String rawPassword){
        return passwordEncoder.encode(rawPassword);
    }

    public static boolean match(String rawPassword,String encodedPassword) {
        return passwordEncoder.matches(rawPassword, encodedPassword);
    }

    public static void main(String[] args) {
       String encodedPass1 = EncrptUtils.encrpt("123456");
       String encodedPass2 = EncrptUtils.encrpt("123456");
       String encodedPass3 = EncrptUtils.encrpt("123456");

        System.out.println(encodedPass1);
        System.out.println(encodedPass2);
        System.out.println(encodedPass3);

        System.out.println(EncrptUtils.match("123456",encodedPass1));
        System.out.println(EncrptUtils.match("123456",encodedPass2));
        System.out.println(EncrptUtils.match("123456",encodedPass3));
    }
}
