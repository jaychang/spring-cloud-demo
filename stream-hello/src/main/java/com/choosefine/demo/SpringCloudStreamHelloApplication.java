package com.choosefine.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.integration.annotation.ServiceActivator;

@EnableBinding(Sink.class)
@SpringBootApplication
public class SpringCloudStreamHelloApplication {

	@ServiceActivator(inputChannel = Sink.INPUT)
	public void receive(Object payload){
		System.out.println("receive "+payload);
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudStreamHelloApplication.class, args);
	}
}
