package com.choosefine.demo.controller;

import com.choosefine.demo.feign.ConsumeTrade3Client;
import com.choosefine.demo.feign.ConsumeTrade4Client;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
 public class Trace2DemoController{
    private static final Logger log = LoggerFactory.getLogger(Trace2DemoController.class);

    @Autowired
    private ConsumeTrade3Client consumeTrade3Client;
    @Autowired
    private ConsumeTrade4Client consumeTrade4Client;

    @GetMapping("bar")
    public String trace2(HttpServletRequest request){
        log.debug("SpanName={},TradeId={},SpanId={},ParentSpanId={}",request.getHeader("x-span-name"),request.getHeader("x-b3-traceid"),request.getHeader("x-b3-spanid"),request.getHeader("x-b3-parentspanid"));
        String trace3Result = consumeTrade3Client.consumeTrade3();
        String trace4Result = consumeTrade4Client.consumeTrade4();
        return trace3Result+"|"+trace4Result;
    }
}
