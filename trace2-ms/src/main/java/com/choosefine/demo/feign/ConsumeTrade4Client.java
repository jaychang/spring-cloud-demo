package com.choosefine.demo.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/4
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@FeignClient(name = "trace4-ms",fallback = ConsumeTrade4Client.ConsumeTrade4Fallback.class)
public interface ConsumeTrade4Client {

    @GetMapping("qux")
    public String consumeTrade4();

    @Component
    static class ConsumeTrade4Fallback implements  ConsumeTrade4Client{

        @Override
        public String consumeTrade4() {
            return "consume trace4 error";
        }
    }
}
