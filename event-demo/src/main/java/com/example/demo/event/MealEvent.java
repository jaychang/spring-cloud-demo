package com.example.demo.event;

import com.example.demo.enums.MealEnum;
import org.springframework.context.ApplicationEvent;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/21
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class MealEvent extends ApplicationEvent {

    private MealEnum mealEnum;

    public MealEvent(Object source) {
        super(source);
    }

    public MealEvent(Object source, MealEnum mealEnum) {
        super(source);
        this.mealEnum = mealEnum;
    }

    public MealEnum getMealEnum() {
        return mealEnum;
    }

    public void setMealEnum(MealEnum mealEnum) {
        this.mealEnum = mealEnum;
    }
}
