package com.example.demo.event;

import org.springframework.context.ApplicationEvent;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/21
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public class TroubleEvent extends ApplicationEvent {
    public TroubleEvent(Object source) {
        super(source);
    }
}
