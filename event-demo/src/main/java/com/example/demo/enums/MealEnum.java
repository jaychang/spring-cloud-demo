package com.example.demo.enums;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/21
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum MealEnum {
    BREAKFAST,LUNCH,DINNER
}
