package com.example.demo;

import com.example.demo.enums.MealEnum;
import com.example.demo.event.MealEvent;
import com.example.demo.event.TroubleEvent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) throws InterruptedException {
		ConfigurableApplicationContext context = SpringApplication.run(DemoApplication.class, args);

		Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					MealEventPublisher mealEventPublisher = context.getBean(MealEventPublisher.class);
					mealEventPublisher.publishMealEvent(new MealEvent("jaychang吃早饭了", MealEnum.BREAKFAST));
					TimeUnit.SECONDS.sleep(1L);
					mealEventPublisher.publishMealEvent(new MealEvent("lucy吃午饭了", MealEnum.LUNCH));
					TimeUnit.SECONDS.sleep(1L);
					mealEventPublisher.publishMealEvent(new MealEvent("jaychang吃晚饭了", MealEnum.DINNER));

                    TroubleEvent troubleEvent = new TroubleEvent("A制造了一个麻烦");

                    TroubleEventPublisher troubleEventPublisher = context.getBean(TroubleEventPublisher.class);
                    troubleEventPublisher.publishTroubleEvent(troubleEvent);

				}catch (InterruptedException e){
					;
				}
			}
		});

        t.setName("demo");
        t.start();
        System.out.println("demo线程启动完成");
        t.join();

        System.out.println("complted.");

//		Runtime.getRuntime().addShutdownHook(new Thread(()->{
//			System.out.println("shutdown...");
//			System.out.println("release system resource....");
//
//		}));
//
//		Thread.sleep(5000);
//
//		System.out.println("complete.");


	}
}
