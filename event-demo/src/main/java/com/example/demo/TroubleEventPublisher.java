package com.example.demo;

import com.example.demo.event.MealEvent;
import com.example.demo.event.TroubleEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/21
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class TroubleEventPublisher implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void publishTroubleEvent(TroubleEvent troubleEvent){
        this.applicationEventPublisher.publishEvent(troubleEvent);
    }
}
