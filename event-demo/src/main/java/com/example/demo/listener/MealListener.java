package com.example.demo.listener;

import com.example.demo.event.MealEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/21
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class MealListener implements ApplicationListener<MealEvent> {

    @Override
    public void onApplicationEvent(MealEvent mealEvent) {
        System.out.println("MealListener.onApplicationEvent");
        switch (mealEvent.getMealEnum()){
            case BREAKFAST:
                processEvent(mealEvent);
                break;
            case LUNCH:
                processEvent(mealEvent);
                break;
            case DINNER:
                processEvent(mealEvent);
                break;
            default:
                System.out.println("发生错误");
        }
    }

    public void processEvent(MealEvent mealEvent){
        System.out.println(String.format("Thread:%s,Type:%s,Event:%s",Thread.currentThread().getName(),mealEvent.getSource(),mealEvent.getMealEnum()));
    }
}
