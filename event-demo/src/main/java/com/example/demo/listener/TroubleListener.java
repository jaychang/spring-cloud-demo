package com.example.demo.listener;

import com.example.demo.event.TroubleEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/21
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class TroubleListener implements ApplicationListener<TroubleEvent> {
    @Override
    public void onApplicationEvent(TroubleEvent troubleEvent) {
        System.out.println("TroubleListener.onApplicationEvent"+troubleEvent.getSource().toString());
    }
}
