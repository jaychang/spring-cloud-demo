package com.choosefine.demo.config;

import com.choosefine.demo.enums.States;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;
import org.springframework.statemachine.transition.Transition;
import org.springframework.stereotype.Component;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
//@Component
public class MyStateListener extends StateMachineListenerAdapter {
    @Override
    public void stateChanged(State from, State to) {
        System.out.println("StateMachineListener.stateChanged");
        System.out.println(from);
        System.out.println(to);
    }

    @Override
    public void transition(Transition transition) {
        if(transition.getSource().getId() == States.UNPAID){
            System.out.println("支付完成，待收货");
            return;
        }
        if(transition.getSource().getId() == States.WAITING_FOR_RECEIVE){
            System.out.println("收获，订单完成");
            return;
        }
    }
}
