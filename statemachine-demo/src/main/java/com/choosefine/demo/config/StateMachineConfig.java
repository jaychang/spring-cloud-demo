package com.choosefine.demo.config;

import com.choosefine.demo.enums.Events;
import com.choosefine.demo.enums.States;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;

import java.util.EnumSet;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Slf4j
@Configuration
@EnableStateMachine
public class StateMachineConfig extends EnumStateMachineConfigurerAdapter<States, Events> {

//    @Autowired
//    private MyStateListener myStateListener;

    @Override
    public void configure(StateMachineStateConfigurer<States, Events> stateMachineStateConfigurer) throws Exception {
        stateMachineStateConfigurer.withStates().initial(States.UNPAID).states(EnumSet.allOf(States.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<States, Events> stateMachineTransitionConfigurer) throws Exception {
        stateMachineTransitionConfigurer
                .withExternal()
                    .source(States.UNPAID)
                    .target(States.WAITING_FOR_RECEIVE)
                    .event(Events.PAY).and()
                .withExternal()
                    .source(States.WAITING_FOR_RECEIVE)
                    .target(States.DONE)
                    .event(Events.RECEIVE);
    }

//    @Override
//    public void configure(StateMachineConfigurationConfigurer<States, Events> stateMachineConfigurationConfigurer) throws Exception {
//        stateMachineConfigurationConfigurer.withConfiguration().listener(myStateListener);
//    }
}
