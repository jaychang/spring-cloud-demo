package com.choosefine.demo.config;

import org.springframework.statemachine.annotation.OnTransition;
import org.springframework.statemachine.annotation.WithStateMachine;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@WithStateMachine
public class EventConfig {
    @OnTransition(target = "UNPAID")
    public void create(){
        System.out.println("订单创建");
    }

    @OnTransition(source = "UNPAID",target = "WAITING_FOR_RECEIVE")
    public void pay(){
        System.out.println("支付完成，等待收货");
    }

    @OnTransition(source = "WAITING_FOR_RECEIVE",target = "DONE")
    public void receive(){
        System.out.println("确认收货，订单完成");
    }
}
