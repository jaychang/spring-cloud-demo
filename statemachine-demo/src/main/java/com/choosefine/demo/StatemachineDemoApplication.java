package com.choosefine.demo;

import com.choosefine.demo.enums.Events;
import com.choosefine.demo.enums.States;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.statemachine.StateMachine;

import java.io.IOException;

@SpringBootApplication
public class StatemachineDemoApplication implements CommandLineRunner {
	@Autowired
	private StateMachine<States,Events> stateMachine;

	public static void main(String[] args) throws IOException {
		SpringApplication.run(StatemachineDemoApplication.class, args);
		System.in.read();
	}

	public void run(String[] args){
		stateMachine.start();
		stateMachine.sendEvent(Events.PAY);
		stateMachine.sendEvent(Events.RECEIVE);
	}
}
