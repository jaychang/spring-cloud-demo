package com.choosefine.demo.enums;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum States {
    UNPAID,                 // 待支付
    WAITING_FOR_RECEIVE,    // 待收货
    DONE                    // 结束
}
