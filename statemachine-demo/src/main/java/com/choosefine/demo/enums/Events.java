package com.choosefine.demo.enums;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/18
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
public enum  Events {
    PAY,            //支付
    RECEIVE        //收货
}
