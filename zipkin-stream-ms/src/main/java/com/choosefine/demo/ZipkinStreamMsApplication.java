package com.choosefine.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.sleuth.zipkin.stream.EnableZipkinStreamServer;
import zipkin.server.EnableZipkinServer;
//HTTP方式
//@EnableZipkinServer
//消息中间件方式
@EnableZipkinStreamServer
@SpringBootApplication
public class ZipkinStreamMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZipkinStreamMsApplication.class, args);
	}
}
