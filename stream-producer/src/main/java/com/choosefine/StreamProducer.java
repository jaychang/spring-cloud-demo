package com.choosefine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.core.MessageSource;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/22
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@EnableBinding(value = {Source.class})
public class StreamProducer {
    private static final Logger log = LoggerFactory.getLogger(StreamProducer.class);

    @Bean
    @InboundChannelAdapter(value = Source.OUTPUT,poller = @Poller(fixedDelay = "2000"))
    public MessageSource timeMessageSource(){
        return new MessageSource<String>() {
            @Override
            public Message receive() {
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String now = sdf.format(new Date());
                return new GenericMessage(String.format("{\"name\":\"jaychang\",\"age\":30,\"now\":%s}",now));
            }
        };
    }
}
