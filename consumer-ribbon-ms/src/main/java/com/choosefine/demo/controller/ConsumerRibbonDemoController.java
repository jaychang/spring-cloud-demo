package com.choosefine.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/25
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class ConsumerRibbonDemoController {
    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("consume")
    public String consumeRibbon(){
        return restTemplate.getForObject("http://provider-ms/services",String.class);
    }
}
