package com.choosefine.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/3
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class Trace5DemoController {
    private static final Logger log = LoggerFactory.getLogger(Trace5DemoController.class);
    @GetMapping("quux")
    public String trace5(HttpServletRequest request){
        log.debug("SpanName={},TradeId={},SpanId={},ParentSpanId={}",request.getHeader("x-span-name"),request.getHeader("x-b3-traceid"),request.getHeader("x-b3-spanid"),request.getHeader("x-b3-parentspanid"));
        return "trace5 quux";
    }
}
