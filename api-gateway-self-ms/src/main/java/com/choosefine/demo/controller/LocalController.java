package com.choosefine.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/14
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class LocalController {
    @GetMapping("local/hello1")
    public String hello1(){
        return "hello1";
    }
}
