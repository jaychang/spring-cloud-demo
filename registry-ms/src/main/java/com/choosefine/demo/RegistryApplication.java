package com.choosefine.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * Hello world!
 */
@EnableEurekaServer
@SpringBootApplication
public class RegistryApplication {
    public static void main( String[] args ) {
        SpringApplication.run(RegistryApplication.class,args);
    }
}
