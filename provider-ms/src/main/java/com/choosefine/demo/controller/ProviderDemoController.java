package com.choosefine.demo.controller;

import org.apache.commons.lang.math.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class ProviderDemoController {
    @Autowired
    private DiscoveryClient discoveryClient;

    @RequestMapping("services")
    public List<String> clientList(){
        //模拟断路器调用依赖的服务时候，时间过长的情况
//        try {
//            Thread.sleep(60000L);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        List<String> serviceIds = discoveryClient.getServices();
        for(String serviceId : serviceIds) {
            List<ServiceInstance> instances = discoveryClient.getInstances(serviceId);
            for (ServiceInstance instance : instances) {
                System.out.println("ServiceId:" + instance.getServiceId() + ",Host:" + instance.getHost() + ",Port:" + instance.getPort() + ",URI:" + instance.getUri() + ",MetaData:" + instance.getMetadata());
            }
        }
        boolean flag = RandomUtils.nextBoolean();
//        if(flag){
//            throw new RuntimeException("xxxx");
//        }
        return serviceIds;
    }
}
