package com.choosefine.demo.feign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/4
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@FeignClient(name = "trace3-stream-ms",fallback = ConsumeTrade3Client.ConsumeTrade3Fallback.class)
public interface ConsumeTrade3Client {
    @GetMapping("baz")
    public String consumeTrade3();

    @Component
    static class ConsumeTrade3Fallback implements ConsumeTrade3Client{

        @Override
        public String consumeTrade3() {
            return "consume trade3 error";
        }
    }
}
