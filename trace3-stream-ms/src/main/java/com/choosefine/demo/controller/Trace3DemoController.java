package com.choosefine.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/2
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class Trace3DemoController {
    private static final Logger log = LoggerFactory.getLogger(Trace3DemoController.class);

    @GetMapping("baz")
    public String trace3(HttpServletRequest request){
        log.debug("SpanName={},TradeId={},SpanId={},ParentSpanId={}",request.getHeader("x-span-name"),request.getHeader("x-b3-traceid"),request.getHeader("x-b3-spanid"),request.getHeader("x-b3-parentspanid"));
        return "trace3 baz";
    }
}
