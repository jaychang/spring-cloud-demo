package com.choosefine.demo.fegin;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/25
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@FeignClient("provider-ms")
public interface ConsumeFegin {
    @GetMapping("services")
    String consumeDemo();
}
