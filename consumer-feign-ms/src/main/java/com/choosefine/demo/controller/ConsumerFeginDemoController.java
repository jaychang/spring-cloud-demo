package com.choosefine.demo.controller;

import com.choosefine.demo.fegin.ConsumeFegin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/25
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class ConsumerFeginDemoController {
    @Autowired
    private ConsumeFegin consumeFegin;

    @GetMapping("consume")
    public String consume(){
        return consumeFegin.consumeDemo();
    }
}
