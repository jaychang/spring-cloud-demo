#服务提供方
##provider-ms

#服务消费Ribbon
##consumer-ribbon-ms

#服务消费Feign
##consumer-feign-ms

#服务消费Ribbon with hystrix服务熔断
##consumer-ribbon-hystrix-ms

#服务消费Feign with hystrix服务熔断
##consumer-feign-hytrix-ms

#Eureka服务注册中心高可用
##registry-ha-ms

#Eureka服务注册中心
##registry-ms

#Zuul网关
##api-gateway-ms

#config-server-ms
##配置服务端服务

#配置客户端服务
##config-client-ms

#单服务熔断监控
##hystrix-dashboard-ms

#服务集群熔断监控
##hystrix-dashboard-turbine-ms




