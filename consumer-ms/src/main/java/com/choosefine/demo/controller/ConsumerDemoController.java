package com.choosefine.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/24
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class ConsumerDemoController {
    @Autowired
    private LoadBalancerClient loadBalancerClient;

    @Autowired
    private RestTemplate restTemplate;

    @RequestMapping("consume")
    public String consumeService(){
        ServiceInstance serviceInstance = loadBalancerClient.choose("provider-ms");
        String url = "http://"+serviceInstance.getHost()+":"+serviceInstance.getPort()+"/"+"services";
        System.out.println(serviceInstance.getUri().toString());
        String result = restTemplate.getForObject(url, String.class);
        return result;
    }
}
