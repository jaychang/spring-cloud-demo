package com.choosefine.demo.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.ZuulFilterResult;
import com.netflix.zuul.context.RequestContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/8/11
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Component
public class AccessFilter extends ZuulFilter {
    private static final Logger logger = LoggerFactory.getLogger(AccessFilter.class);
    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 0;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() {
        RequestContext currentContext = RequestContext.getCurrentContext();
        HttpServletRequest request = currentContext.getRequest();
        String accessToken = request.getHeader("accessToken");
        if("123456".equals(accessToken)){
            logger.debug("accessToken is ok.");
            return null;
        }
        logger.debug("accessToken is not offered or wrong.");
        currentContext.setResponseStatusCode(HttpStatus.UNAUTHORIZED.value());
        currentContext.setSendZuulResponse(false);
        currentContext.setResponseBody("{\"code\":401,\"message\":\"accessToken is not offered or wrong.\"}");
        return null;
    }
}
