package com.choosefine.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@SpringBootApplication
public class ApiGatewayMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGatewayMsApplication.class, args);
	}
}
