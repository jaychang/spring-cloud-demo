package com.choosefine.demo.feign;

import com.choosefine.demo.feign.ConsumeFeign.ConsumeFeignFallback;
import com.choosefine.demo.feign.ConsumeFeign.ConsumeFeignFallbackFactory;
import feign.hystrix.FallbackFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/28
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
//@FeignClient(name = "provider-ms",fallback = ConsumeFeignFallback.class)
@FeignClient(name = "provider-ms",fallbackFactory = ConsumeFeignFallbackFactory.class)
public interface ConsumeFeign {
    @GetMapping("services")
    public String consumeByFeign();

    @Component
    static class ConsumeFeignFallback implements ConsumeFeign{
        @Override
        public String consumeByFeign() {
            return "An error occurs,can not get service instances list";
        }
    }

    @Component
    static class  ConsumeFeignFallbackFactory implements FallbackFactory{
        private static final Logger log = LoggerFactory.getLogger(ConsumeFeignFallbackFactory.class);

        @Override
        public Object create(Throwable throwable) {
            return new ConsumeFeign(){
                @Override
                public String consumeByFeign() {
                    log.error("fallback;reason was: ",throwable);
                    return "An error occurs,can not get service instances list";
                }
            };
        }
    }
}
