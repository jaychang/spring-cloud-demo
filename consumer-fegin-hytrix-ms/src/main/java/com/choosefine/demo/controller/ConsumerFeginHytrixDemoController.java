package com.choosefine.demo.controller;

import com.choosefine.demo.feign.ConsumeFeign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/28
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class ConsumerFeginHytrixDemoController {
    //这里当然可以再弄一层Service层，为了简单演示就不这样做了
    @Autowired
    private ConsumeFeign consumeFeign;

    @GetMapping("hello")
    public String hello(){
        return consumeFeign.consumeByFeign();
    }
}
