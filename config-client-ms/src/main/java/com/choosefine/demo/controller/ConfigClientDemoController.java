package com.choosefine.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Comments： 这边的@RefreshScope注解不能少，否则即使调用/refresh，配置也不会刷新
 * Author：Jay Chang
 * Create Date：2017/7/27
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RefreshScope
@RestController
public class ConfigClientDemoController {
    @Value("${ccb.wlptUrl}")
    private String ccbWlptUrl;

    @GetMapping("hello")
    public String hello(){
        return ccbWlptUrl;
    }

}
