package com.choosefine.demo.controller;

import com.choosefine.demo.service.RibbonHytrixDemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/28
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@RestController
public class ConsumerRibbonHytrixDemoController {
    @Autowired
    private RibbonHytrixDemoService ribbonHytrixDemoService;

    @GetMapping("hello")
    public String hello(){
        return ribbonHytrixDemoService.consumeByRibbon();
    }
}
