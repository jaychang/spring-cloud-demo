package com.choosefine.demo.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.RestTemplate;

/**
 * Comments：
 * Author：Jay Chang
 * Create Date：2017/7/28
 * Modified By：
 * Modified Date：
 * Why & What is modified：
 * Version：v1.0
 */
@Service
public class RibbonHytrixDemoService {
    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "fallback",
        commandProperties = {
            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value="5000"),
            @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds",value="10000"),
        },
        threadPoolProperties = {
            @HystrixProperty(name="coreSize",value = "4"),
            @HystrixProperty(name="maxQueueSize",value = "3"),
        })
    public String consumeByRibbon(){
        return restTemplate.getForObject("http://provider-ms/services",String.class);
    }

    public String fallback(){
        return "An error occurs,can not get service instances list";
    }
}
