package com.choosefine.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class Trace4StreamMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Trace4StreamMsApplication.class, args);
	}
}
